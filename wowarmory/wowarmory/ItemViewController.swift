//
//  ItemViewController.swift
//  wowarmory
//
//  Created by admin on 09.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ItemViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var itemLevelLabel: UILabel!
    @IBOutlet weak var itemClassLabel: UILabel!
    @IBOutlet weak var itemSubClassLabel: UILabel!
    @IBOutlet weak var statsTextView: UITextView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var closePopoverButton: UIButton!
    
    var item: JSON = JSON.nullJSON
    var data: JSON = JSON.nullJSON
    var itemClassesData: JSON = JSON.nullJSON
    var itemId: Int = 0 {
        didSet{
            getItem()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
    }
    
    func updateUI(){
        statsTextView.sizeToFit()
        statsTextView.layoutIfNeeded()
        self.statsTextView.text = ""
        spinner.stopAnimating()
        toggleGUIElements()
        
        setItemHeader()
        setItemNameColor()
        setItemClass()
        setWeaponInfo()
        setArmor()
        setStats()
        setSellPrice()
        setRequiredLevel()
        
        statsTextView.font = UIFont.systemFontOfSize(15)
    }
    
    private func toggleGUIElements(){
        spinner.hidden = true
        nameLabel.hidden = false
        itemLevelLabel.hidden = false
        statsTextView.hidden = false
        closePopoverButton.hidden = false
    }
    
    private func getItem(){
        Alamofire.request(.GET, RequestParameter.URL.Item + self.itemId.description + RequestParameter.URL.Locale + RequestParameter.URL.ApiKey).responseJSON{ (request, response, json, error) in
                if json != nil {
                    self.data = JSON(json!)
                    self.getItemClasses()
                }
        }
    }
    
    private func getItemClasses(){
            Alamofire.request(.GET, RequestParameter.URL.Item + RequestParameter.URL.Locale + RequestParameter.URL.ApiKey).responseJSON{ (request, response, json, error) in
                if json != nil {
                    self.itemClassesData = JSON(json!)
                    self.updateUI()
                }
        }
    }
    
    private func setItemHeader(){
        self.nameLabel.text = item["name"].description
        let itemLevel = item["itemLevel"].description
        self.itemLevelLabel.text = ItemInfo.ItemLevel + item["itemLevel"].description
    }
    
    private func setItemNameColor(){
        let quality = item["quality"]
        switch quality.intValue {
        case ItemInfo.EpicQuality:
            self.nameLabel.textColor = UIColor.purpleColor()
        case ItemInfo.LegendaryQuality:
            self.nameLabel.textColor = UIColor.orangeColor()
        default:
            self.nameLabel.textColor = UIColor.blackColor()
        }
    }
    
    private func setWeaponInfo(){
        let weaponInfo = item["weaponInfo"]
        if weaponInfo.description != "null" {
            self.statsTextView.text = self.statsTextView.text + weaponInfo["damage"]["min"].description + " - " + weaponInfo["damage"]["max"].description + " " + ItemInfo.Damage + ItemInfo.NewLine
            self.statsTextView.text = self.statsTextView.text + weaponInfo["weaponSpeed"].description + " " + ItemInfo.AttackSpeed + ItemInfo.NewLine
            self.statsTextView.text = self.statsTextView.text + "(" + weaponInfo["dps"].intValue.description + " " + ItemInfo.DPS + ")" + ItemInfo.NewLine
        }
    }
    
    private func setItemClass(){
        let itemClassNumber = data["itemClass"].description
        let itemSubClassNumber = data["itemSubClass"].description
        let itemClass = itemClassesData["classes"]
        if itemClass.count > 0 {
            for var i = 0; i < itemClass.count; i++ {
                if itemClass[i]["class"].description == itemClassNumber {
                    var itemClassName = itemClass[i]["name"]
                    let subClasses = itemClass[i]["subclasses"]
                    for var j = 0; j < subClasses.count; j++ {
                        if subClasses[j]["subclass"].description == itemSubClassNumber{
                            self.statsTextView.text = self.statsTextView.text + subClasses[j]["name"].description + ItemInfo.NewLine
                        }
                    }
                }
            }
        }
    }
    
    private func setStats(){
        let stats = item["stats"]
        if stats.count > 0 {
            for var i = 0; i < stats.count; i++ {
                var amount = stats[i]["amount"]
                var stat = stats[i]["stat"]
                var statName = ItemInfo.StatUnknown
                if let stat = ItemInfo.Stats[stat.intValue]{
                    statName = stat
                }
                self.statsTextView.text = self.statsTextView.text + ItemInfo.Plus + amount.description + ItemInfo.Space
                self.statsTextView.text = self.statsTextView.text + statName + ItemInfo.NewLine
            }
        } else {
            let bonusStats = item["bonusStats"]
            if bonusStats.count > 0 {
                for var i = 0; i < bonusStats.count; i++ {
                    var amount = bonusStats[i]["amount"]
                    var stat = bonusStats[i]["stat"]
                    var statName = ItemInfo.StatUnknown
                    if let stat = ItemInfo.Stats[stat.intValue]{
                        statName = stat
                    }
                    self.statsTextView.text = self.statsTextView.text + ItemInfo.Plus + amount.description + ItemInfo.Space
                    self.statsTextView.text = self.statsTextView.text + statName + ItemInfo.NewLine
                }
            }
        }
    }
    
    private func setArmor(){
        let armor = item["armor"]
        if armor > 0 {
            self.statsTextView.text = armor.description + ItemInfo.Space + ItemInfo.BasedArmor + ItemInfo.NewLine
        } else {
            let baseArmor = item["baseArmor"]
            if baseArmor > 0 {
                self.statsTextView.text = baseArmor.description + ItemInfo.Space + ItemInfo.BasedArmor + ItemInfo.NewLine
            }
        }
    }

    private func setSellPrice(){
        let sellPrice = data["sellPrice"].description
        let sellPriceInt = sellPrice.toInt()
        if sellPriceInt > 0 {
            // calculate item price
            let gold = sellPriceInt! / 10000
            let silver = (sellPriceInt! - (gold * 10000)) / 100
            let copper = sellPriceInt! % 100
            if sellPriceInt < 100 {
                self.statsTextView.text = self.statsTextView.text + ItemInfo.SellPrice + copper.description + "K" + ItemInfo.NewLine
            } else if sellPriceInt < 10000{
                self.statsTextView.text = self.statsTextView.text + ItemInfo.SellPrice + silver.description + "S " + copper.description + "K" + ItemInfo.NewLine
            } else {
                self.statsTextView.text = self.statsTextView.text + ItemInfo.SellPrice + gold.description + "G " + silver.description + "S " + copper.description + "K" + ItemInfo.NewLine
            }
        }
    }
    
    private func setRequiredLevel(){
        let requiredLevel = data["requiredLevel"].description
        if requiredLevel != "null" {
            self.statsTextView.text = self.statsTextView.text + ItemInfo.RequiredLevel + data["requiredLevel"].description
        }
    }
    
    @IBAction func closePopover() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private struct ItemInfo {
        static let ItemLevel = "Gegenstandsstufe "
        static let SellPrice = "Verkaufspreis: "
        static let RequiredLevel = "Erfodert Stufe "
        static let BasedArmor = "Armor"
        static let Damage = "Schaden"
        static let AttackSpeed = "Geschwindigkeit"
        static let StatUnknown = "Unkown"
        static let DPS = "Schaden pro Sekunde"
        
        static let EpicQuality = 4
        static let LegendaryQuality = 5
        
        static let Plus = "+"
        static let NewLine = "\n"
        static let Distance = "\t"
        static let Space = " "
        static let Stats = [3:"Beweglichkeit",4:"Stärke",5:"Intelligenz",6:"Willenskraft",7:"Ausdauer",13:"Ausweichchance",32:"kritische Trefferchance",35:"PvP-Abhärtung",36:"Tempowertung",40:"Vielseitigkeit",45:"Zaubermacht",49:"Meisterschaftswertung",50:"Bonusrüstung",57:"PvP-Macht",59:"Mehrfachschlag",62:"Lebensraub",63:"Vermeidung",73:"Beweglichkeit oder Intelligenz",74:"Stärke oder Intelligenz"]
    }
}
