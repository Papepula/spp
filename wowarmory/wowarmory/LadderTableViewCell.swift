//
//  LadderTableViewCell.swift
//  wowarmory
//
//  Created by admin on 26.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit

class LadderTableViewCell: UITableViewCell {

    @IBOutlet weak var rankingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var winsLabel: UILabel!
    @IBOutlet weak var lossesLabel: UILabel!
    @IBOutlet weak var realmLabel: UILabel!
    @IBOutlet weak var colonLabel: UILabel!
}
