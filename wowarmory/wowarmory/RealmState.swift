//
//  RealmState.swift
//  wowarmory
//
//  Created by admin on 15.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class RealmState {
    
    var name: String = ""
    var type: String = ""
    var population: String = ""
    var status: String = ""
    
    init(name: String, type: String, population: String, status: String){
        self.name = name
        self.type = type
        self.population = population
        self.status = status
    }
}