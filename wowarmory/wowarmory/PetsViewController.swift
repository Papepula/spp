//
//  PetsViewController.swift
//  wowarmory
//
//  Created by admin on 11.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PetsViewController: UIViewController {

    @IBOutlet weak var name1TextLabel: UILabel!
    @IBOutlet weak var name2TextLabel: UILabel!
    @IBOutlet weak var name3TextLabel: UILabel!
    @IBOutlet weak var level1TextLabel: UILabel!
    @IBOutlet weak var level2TextLabel: UILabel!
    @IBOutlet weak var level3TextLabel: UILabel!
    @IBOutlet weak var health1TextLabel: UILabel!
    @IBOutlet weak var health2TextLabel: UILabel!
    @IBOutlet weak var health3TextLabel: UILabel!
    @IBOutlet weak var power1TextLabel: UILabel!
    @IBOutlet weak var power2TextLabel: UILabel!
    @IBOutlet weak var power3TextLabel: UILabel!
    @IBOutlet weak var speed1TextLabel: UILabel!
    @IBOutlet weak var speed2TextLabel: UILabel!
    @IBOutlet weak var speed3TextLabel: UILabel!
    @IBOutlet weak var pet1ImageView: UIImageView!
    @IBOutlet weak var pet2ImageView: UIImageView!
    @IBOutlet weak var pet3ImageView: UIImageView!
    
    @IBOutlet weak var health1ImageView: UIImageView!
    @IBOutlet weak var health2ImageView: UIImageView!
    @IBOutlet weak var health3ImageView: UIImageView!
    @IBOutlet weak var attackpower1ImageView: UIImageView!
    @IBOutlet weak var attackpower2ImageView: UIImageView!
    @IBOutlet weak var attackpower3ImageView: UIImageView!
    @IBOutlet weak var attackspeed1ImageView: UIImageView!
    @IBOutlet weak var attackspeed2ImageView: UIImageView!
    @IBOutlet weak var attackspeed3ImageView: UIImageView!
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var requestFailedTextLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var allPetsData: JSON = JSON.nullJSON
    var selectedPetsData: JSON = JSON.nullJSON
    var selectedPetsIds = [String]()
    var pets = [Pet]()
    var petImages = [UIImage]()
    
    var name: String = ""
    var realm: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if name != "" && realm != "" {
            spinner.startAnimating()
            getSelectedPets()
        } else {
            requestFailed()
        }
    }
    
    func updateUI(){
        setIdsFromSelectedPets()
        let showPetsQueue = dispatch_queue_create("asyncUpdateUI",nil)
        dispatch_async(showPetsQueue) {
            self.setPetInformations()
            self.test()
        }
        
    }
    
    private func test(){
        dispatch_async(dispatch_get_main_queue()){
            self.spinner.stopAnimating()
            self.spinner.hidden = true
            
            switch self.pets.count {
            case 3:
                self.setPet1()
                self.setPet2()
                self.setPet3()
                self.showPet1Elements()
                self.showPet2Elements()
                self.showPet3Elements()
            case 2:
                self.setPet1()
                self.setPet2()
                self.showPet1Elements()
                self.showPet2Elements()
            case 1:
                self.setPet1()
                self.showPet1Elements()
            default:
                self.requestFailed()
            }
        }
    }
    
    private func updateImages(){
        switch petImages.count {
        case 3:
            setImage1()
            setImage2()
            setImage3()
        case 2:
            setImage1()
            setImage2()
        case 1:
            setImage1()
        default:
            requestFailed()
        }
    }
    
    private func setImage1(){
        pet1ImageView.image = petImages[0]
    }
    
    private func setImage2(){
        pet2ImageView.image = petImages[1]
    }
    
    private func setImage3(){
        pet3ImageView.image = petImages[2]
    }
    
    private func setPet1(){
        name1TextLabel.text = pets[0].name
        level1TextLabel.text = PetInfo.Level + PetInfo.Space + pets[0].level.description
        health1TextLabel.text = pets[0].health.description
        power1TextLabel.text = pets[0].power.description
        speed1TextLabel.text = pets[0].speed.description
        getPetIcon(pets[0].icon)
        showPet1Elements()
    }
    
    private func setPet2(){
        name2TextLabel.text = pets[1].name
        level2TextLabel.text = PetInfo.Level + PetInfo.Space + pets[1].level.description
        health2TextLabel.text = pets[1].health.description
        power2TextLabel.text = pets[1].power.description
        speed2TextLabel.text = pets[1].speed.description
        getPetIcon(pets[1].icon)
        showPet2Elements()
        firstView.hidden = false
    }
    
    private func setPet3(){
        name3TextLabel.text = pets[2].name
        level3TextLabel.text = PetInfo.Level + PetInfo.Space + pets[2].level.description
        health3TextLabel.text = pets[2].health.description
        power3TextLabel.text = pets[2].power.description
        speed3TextLabel.text = pets[2].speed.description
        getPetIcon(pets[2].icon)
        showPet3Elements()
        secondView.hidden = false
    }
    
    private func showPet1Elements(){
        name1TextLabel.hidden = false
        level1TextLabel.hidden = false
        health1TextLabel.hidden = false
        power1TextLabel.hidden = false
        speed1TextLabel.hidden = false
        attackpower1ImageView.hidden = false
        attackspeed1ImageView.hidden = false
        health1ImageView.hidden = false
        pet1ImageView.hidden = false
    }
    
    private func showPet2Elements(){
        name2TextLabel.hidden = false
        level2TextLabel.hidden = false
        health2TextLabel.hidden = false
        power2TextLabel.hidden = false
        speed2TextLabel.hidden = false
        attackpower2ImageView.hidden = false
        attackspeed2ImageView.hidden = false
        health2ImageView.hidden = false
        pet2ImageView.hidden = false
    }
    
    private func showPet3Elements(){
        name3TextLabel.hidden = false
        level3TextLabel.hidden = false
        health3TextLabel.hidden = false
        power3TextLabel.hidden = false
        speed3TextLabel.hidden = false
        attackpower3ImageView.hidden = false
        attackspeed3ImageView.hidden = false
        health3ImageView.hidden = false
        pet3ImageView.hidden = false
    }
    
    private func setPetInformations(){
        var allPets = allPetsData["pets"]["collected"]
        if allPets.count > 0 {
            for var i = 0; i < allPets.count; i++ {
                for var j = 0; j < self.selectedPetsIds.count; j++ {
                    if allPets[i]["battlePetGuid"].description == self.selectedPetsIds[j]{
                        var name = allPets[i]["name"]
                        var quality = allPets[i]["quality"]
                        var icon = allPets[i]["icon"]
                        var speciesId = allPets[i]["stats"]["speciesId"]
                        var breedId = allPets[i]["stats"]["breedId"]
                        var level = allPets[i]["stats"]["level"]
                        var health = allPets[i]["stats"]["health"]
                        var power = allPets[i]["stats"]["power"]
                        var speed = allPets[i]["stats"]["speed"]
                        var creatureName = allPets[i]["creatureName"]
                        var firstAbilty = allPets[i]["isFirstAbilitySlotSelected"]
                        var secondAbilty = allPets[i]["isSecondAbilitySlotSelected"]
                        var thirdAbility = allPets[i]["isThirdAbilitySlotSelected"]
                        self.pets.append(Pet(name: name.description, qualityId: quality.intValue, icon: icon.description, speciesId: speciesId.intValue, breedId: breedId.intValue, level: level.intValue, health: health.intValue, power: power.intValue, speed: speed.intValue, creatureName: creatureName.description, firstAbility: firstAbilty.boolValue, secondAbility: secondAbilty.boolValue, thirdAbility: thirdAbility.boolValue))
                    }
                }
            }
        }else {
            requestFailed()
        }
    }
    
    private func setIdsFromSelectedPets(){
        let selectedPets = selectedPetsData["petSlots"]
        if selectedPets.count > 0 {
            for var i = 0; i < selectedPets.count; i++ {
                var id = selectedPets[i]["battlePetGuid"].description
                selectedPetsIds.append(id)
            }
        } else {
            requestFailed()
        }
    }
    
    private func getAllPets(){
        var urlString = RequestParameter.URL.Character + self.realm + "/" + self.name + RequestParameter.URL.Pets + RequestParameter.URL.AndLocale + RequestParameter.URL.ApiKey
        urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        Alamofire.request(.GET, urlString)
            .responseJSON{ (request, response, json, error) in
                if json != nil {
                    var jsonObj = JSON(json!)
                    self.allPetsData = jsonObj
                    self.updateUI()
                }
        }
    }
    
    private func getSelectedPets(){
        var urlString = RequestParameter.URL.Character + self.realm + "/" + self.name + RequestParameter.URL.PetSlots + RequestParameter.URL.AndLocale + RequestParameter.URL.ApiKey
        urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        Alamofire.request(.GET, urlString)
            .responseJSON{ (request, response, json, error) in
                if json != nil {
                    var jsonObj = JSON(json!)
                    self.selectedPetsData = jsonObj
                    self.getAllPets()
                }
        }
    }
    
    private func getPetIcon(petPath: String){
        let downloadQueue = dispatch_queue_create("com.pro.asyncImages",nil)
        dispatch_async(downloadQueue){
            var data = NSData(contentsOfURL: NSURL(string: RequestParameter.URL.PetImage + petPath + RequestParameter.URL.JPG)!)
            var image: UIImage?
            if (data != nil){
                image = UIImage(data: data!)
            }
            dispatch_async(dispatch_get_main_queue()){
                self.petImages.append(image!)
                self.updateImages()
            }
        }
    }
    
    private func requestFailed(){
        requestFailedTextLabel.text = RequestParameter.URL.PetRequestFailed
        requestFailedTextLabel.hidden = false
    }
    
    private struct PetInfo {
        static let Level = "Stufe"
        static let Space = " "
    }
}
