//
//  LadderTableViewController.swift
//  wowarmory
//
//  Created by admin on 26.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LadderTableViewController: UITableViewController {

    @IBOutlet var ladderTableView: UITableView!
    @IBOutlet weak var spinner: UIRefreshControl!
    var requestData: JSON = JSON.nullJSON
    var bracketType: String = ""
    var ladder: JSON = JSON.nullJSON
    var name: String = ""
    var realm: String = ""
    var ladderCharacters: [LadderCharacter] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        executeRequest()
    }
    
    private func executeRequest(){
       spinner.beginRefreshing()
        if bracketType != ""{
            Alamofire.request(.GET, RequestParameter.URL.Leaderboard + self.bracketType + RequestParameter.URL.Locale + RequestParameter.URL.ApiKey).responseJSON{ (request, response, json, error) in
                    if json != nil {
                        var jsonObj = JSON(json!)
                        self.requestData = jsonObj
                        self.ladder = self.requestData["rows"]
                        self.setLadderModel()
                        self.ladderTableView.reloadData()
                        self.spinner.endRefreshing()
                    }
            }
        }
    }
    
    @IBAction func refresh(sender: UIRefreshControl) {
        executeRequest()
    }
    
    private func setLadderModel(){
        for var i = 0; i < Ladder.AmountOfShownCharacters; i++ {
            var name = ladder[i][Ladder.Name]
            var realm = ladder[i][Ladder.Realm]
            var seasonWins = ladder[i][Ladder.SeasonWins]
            var seasonLosses = ladder[i][Ladder.SeasonLosses]
            var rating = ladder[i][Ladder.Rating]
            var raceId = ladder[i][Ladder.RaceId]
            var classId = ladder[i][Ladder.ClassId]
            var fractionId = ladder[i][Ladder.FractionId]
            var ranking = ladder[i][Ladder.Ranking]
            self.ladderCharacters.append(LadderCharacter(name: name.description, realm: realm.description, seasonWins: seasonWins.intValue, seasonLosses: seasonLosses.intValue, rating: rating.intValue, raceId: raceId.intValue, classId: classId.intValue, fractionId: fractionId.intValue, ranking: ranking.intValue))
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Ladder.AmountOfShownCharacters
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CustomCell.Ladder, forIndexPath: indexPath) as! LadderTableViewCell
        let row = indexPath.row
        if ladder != nil {
            cell.nameLabel.text = ladderCharacters[row].name
            cell.rankingLabel.text = ladderCharacters[row].ranking.description
            cell.ratingLabel.text = ladderCharacters[row].rating.description
            cell.winsLabel.text = ladderCharacters[row].seasonWins.description
            cell.lossesLabel.text = ladderCharacters[row].seasonLosses.description
            cell.realmLabel.text = ladderCharacters[row].realm
            cell.colonLabel.text = Ladder.Colon
            if ladderCharacters[row].fractionId == Ladder.Horde{
                cell.nameLabel.textColor = UIColor.redColor()
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.dequeueReusableCellWithIdentifier(CustomCell.Ladder, forIndexPath: indexPath) as! LadderTableViewCell
        let row = indexPath.row
        self.name = ladderCharacters[row].name
        self.realm = ladderCharacters[row].realm
        performSegueWithIdentifier(Segue.SearchLadderCharacter, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if self.name != "" && self.realm != "" {
            if segue.identifier == Segue.SearchLadderCharacter {
                if let navcon = segue.destinationViewController as? UINavigationController{
                    if let scvc = navcon.topViewController as? ShowCharacterViewController{
                        scvc.characterName = self.name
                        scvc.characterRealm = self.realm
                    }
                }
            }
        }
    }
    
    private struct Segue {
        static let SearchLadderCharacter = "Search Ladder Character"
    }
    
    private struct CustomCell {
        static let Ladder = "Ladder"
    }
    
    private struct Ladder {
        static let AmountOfShownCharacters = 100
        static let Horde = 1
        static let Alliance = 0
        static let Colon = ":"
        static let Name = "name"
        static let Realm = "realmName"
        static let SeasonWins = "seasonWins"
        static let SeasonLosses = "seasonLosses"
        static let Rating = "rating"
        static let RaceId = "raceId"
        static let ClassId = "classId"
        static let FractionId = "fractionId"
        static let Ranking = "ranking"
    }
}
