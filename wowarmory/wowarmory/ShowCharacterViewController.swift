//
//  ShowCharacterViewController.swift
//  wowarmory
//
//  Created by admin on 25.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ShowCharacterViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet var characterView: UIView!
    @IBOutlet weak var headButton: UIButton!
    @IBOutlet weak var neckButton: UIButton!
    @IBOutlet weak var shoulderButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var chestButton: UIButton!
    @IBOutlet weak var shirtButton: UIButton!
    @IBOutlet weak var tabardButton: UIButton!
    @IBOutlet weak var wristsButton: UIButton!
    @IBOutlet weak var handsButton: UIButton!
    @IBOutlet weak var waistButton: UIButton!
    @IBOutlet weak var legsButton: UIButton!
    @IBOutlet weak var feetsButton: UIButton!
    @IBOutlet weak var finger1Button: UIButton!
    @IBOutlet weak var finger2Button: UIButton!
    @IBOutlet weak var trinket1Button: UIButton!
    @IBOutlet weak var trinket2Button: UIButton!
    @IBOutlet weak var mainHandButton: UIButton!
    @IBOutlet weak var offHandButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var raceLabel: UILabel!

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var addToFavouriteImageView: UIImageView!
    @IBOutlet weak var showPetsButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var requestFailedTextLabel: UILabel!
    
    var equipment = [String: UIImage]()
    var characterName: String = ""
    var characterRealm: String = ""
    var data: JSON = JSON.nullJSON
    var classesData: JSON = JSON.nullJSON
    let races: [CharacterRace] = CharacterRaces().characterRaces
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        executeRequest()
    }
    
    override func viewWillAppear(animated: Bool) {
        let currentCharacter = SearchCharacter(name: self.characterName, realm: self.characterRealm)
        var favoriteArray = loadFavorites()
        
        if favoritesAlreadyContainsObject(favoriteArray, newFavorite: currentCharacter) == true{
            self.addToFavouriteImageView.image = UIImage(named: (Favorite.YellowStar))
        }else{
            self.addToFavouriteImageView.image = UIImage(named: (Favorite.WhiteStar))
        }
    }
    
    private func executeRequest(){
        
        var urlString = RequestParameter.URL.Character + self.characterRealm + "/" + self.characterName + RequestParameter.URL.Items + RequestParameter.URL.AndLocale + RequestParameter.URL.ApiKey
        urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        if characterName != "" && characterRealm != "" {
            Alamofire.request(.GET, urlString)
                .responseJSON{ (request, response, json, error) in
                    if json != nil {
                        var jsonObj = JSON(json!)
                        if jsonObj["status"].description == RequestParameter.URL.RequestStatus {
                            self.characterNotFound()
                        }else{
                            self.data = jsonObj
                            self.characterFound()
                            self.updateUI()
                        }
                    }
            }
        } else {
            characterNotFound()
        }
    }
    
    private func characterNotFound(){
        requestFailedTextLabel.text = RequestParameter.URL.CharacterNotFound
        requestFailedTextLabel.hidden = false
        spinner.hidden = true
        spinner.stopAnimating()
    }
    
    private func characterFound(){
        addToFavouriteImageView.hidden = false
        nameLabel.hidden = false
        levelLabel.hidden = false
        classLabel.hidden = false
        raceLabel.hidden = false
        profileImageView.hidden = false
        mainHandButton.hidden = false
        offHandButton.hidden = false
        headButton.hidden = false
        neckButton.hidden = false
        shoulderButton.hidden = false
        backButton.hidden = false
        chestButton.hidden = false
        shirtButton.hidden = false
        tabardButton.hidden = false
        wristsButton.hidden = false
        handsButton.hidden = false
        waistButton.hidden = false
        legsButton.hidden = false
        feetsButton.hidden = false
        finger1Button.hidden = false
        finger2Button.hidden = false
        trinket1Button.hidden = false
        trinket2Button.hidden = false
    }
    
    func updateUI(){
        spinner.stopAnimating()
        spinner.hidden = true
        showPetsButton.hidden = false
        
        var thumbnail = data[Character.Thumbnail].string

        if let level = data[Character.Level].int {
            levelLabel.text = level.description
        }
        if let name = data[Character.Name].string {
            nameLabel.text = name
        }
        if let race = data[Character.Race].int {
            raceLabel.text = getCharacterRace(race)
        }
        if let charClass = data[Character.Class].int {
            classLabel.text = Character.Classes[charClass]
        }
        
        showItemImage()
        loadCharacterImage()
    }
    
    private func getCharacterRace(race: Int) -> String{
        for var i = 0; i < self.races.count; i++ {
            if races[i].id == race{
                return races[i].name
            }
        }
        return ItemSlot.Error
    }
    
    private func showItemImage(){
        let headPath = data[ItemSlot.Items][ItemSlot.Head][ItemSlot.Icon]
        if itemEquipped(headPath.description) == true {
            loadItemImage(ItemSlot.Head, itemPath: headPath.description)
        }else {
            headButton.enabled = false
        }
        
        let neckPath = data[ItemSlot.Items][ItemSlot.Neck][ItemSlot.Icon]
        if itemEquipped(neckPath.description) == true {
            loadItemImage(ItemSlot.Neck, itemPath: neckPath.description)
        }else {
            neckButton.enabled = false
        }
        
        let shoulderPath = data[ItemSlot.Items][ItemSlot.Shoulder][ItemSlot.Icon]
        if itemEquipped(shoulderPath.description) == true {
            loadItemImage(ItemSlot.Shoulder, itemPath: shoulderPath.description)
        }else {
            shoulderButton.enabled = false
        }
        
        let backPath = data[ItemSlot.Items][ItemSlot.Back][ItemSlot.Icon]
        if itemEquipped(backPath.description) == true {
            loadItemImage(ItemSlot.Back, itemPath: backPath.description)
        }else {
            backButton.enabled = false
        }
        
        let chestPath = data[ItemSlot.Items][ItemSlot.Chest][ItemSlot.Icon]
        if itemEquipped(chestPath.description) == true {
            loadItemImage(ItemSlot.Chest, itemPath: chestPath.description)
        }else {
            chestButton.enabled = false
        }
        
        let shirtPath = data[ItemSlot.Items][ItemSlot.Shirt][ItemSlot.Icon]
        if itemEquipped(shirtPath.description) == true {
            loadItemImage(ItemSlot.Shirt, itemPath: shirtPath.description)
        }else {
            shirtButton.enabled = false
        }
        
        let tabardPath = data[ItemSlot.Items][ItemSlot.Tabard][ItemSlot.Icon]
        if itemEquipped(tabardPath.description) == true {
            loadItemImage(ItemSlot.Tabard, itemPath: tabardPath.description)
        }else {
            tabardButton.enabled = false
        }
        
        let wristPath = data[ItemSlot.Items][ItemSlot.Wrists][ItemSlot.Icon]
        if itemEquipped(wristPath.description) == true {
            loadItemImage(ItemSlot.Wrists, itemPath: wristPath.description)
        }else {
            wristsButton.enabled = false
        }
        
        let handsPath = data[ItemSlot.Items][ItemSlot.Hands][ItemSlot.Icon]
        if itemEquipped(handsPath.description) == true {
            loadItemImage(ItemSlot.Hands, itemPath: handsPath.description)
        }else {
            handsButton.enabled = false
        }
        
        let waistPath = data[ItemSlot.Items][ItemSlot.Waist][ItemSlot.Icon]
        if itemEquipped(waistPath.description) == true {
            loadItemImage(ItemSlot.Waist, itemPath: waistPath.description)
        }else {
            waistButton.enabled = false
        }
        
        let legsPath = data[ItemSlot.Items][ItemSlot.Legs][ItemSlot.Icon]
        if itemEquipped(legsPath.description) == true {
            loadItemImage(ItemSlot.Legs, itemPath: legsPath.description)
        }else {
            legsButton.enabled = false
        }
        
        let feetPath = data[ItemSlot.Items][ItemSlot.Feets][ItemSlot.Icon]
        if itemEquipped(feetPath.description) == true {
            loadItemImage(ItemSlot.Feets, itemPath: feetPath.description)
        }else {
            feetsButton.enabled = false
        }
        
        let finger1Path = data[ItemSlot.Items][ItemSlot.Finger1][ItemSlot.Icon]
        if itemEquipped(finger1Path.description) == true {
            loadItemImage(ItemSlot.Finger1, itemPath: finger1Path.description)
        }else {
            finger1Button.enabled = false
        }
        
        let finger2Path = data[ItemSlot.Items][ItemSlot.Finger2][ItemSlot.Icon]
        if itemEquipped(finger2Path.description) == true {
            loadItemImage(ItemSlot.Finger2, itemPath: finger2Path.description)
        }else {
            finger2Button.enabled = false
        }
        
        let trinket1Path = data[ItemSlot.Items][ItemSlot.Trinket1][ItemSlot.Icon]
        if itemEquipped(trinket1Path.description) == true {
            loadItemImage(ItemSlot.Trinket1, itemPath: trinket1Path.description)
        }else {
            trinket1Button.enabled = false
        }
        
        let trinket2Path = data[ItemSlot.Items][ItemSlot.Trinket2][ItemSlot.Icon]
        if itemEquipped(trinket2Path.description) == true {
            loadItemImage(ItemSlot.Trinket2, itemPath: trinket2Path.description)
        }else {
            trinket2Button.enabled = false
        }
        
        let mainHandPath = data[ItemSlot.Items][ItemSlot.MainHand][ItemSlot.Icon]
        if itemEquipped(mainHandPath.description) == true {
            loadItemImage(ItemSlot.MainHand, itemPath: mainHandPath.description)
        }else {
            mainHandButton.enabled = false
        }
    
        let offHandPath = data[ItemSlot.Items][ItemSlot.OffHand][ItemSlot.Icon]
        if itemEquipped(offHandPath.description) == true {
            loadItemImage(ItemSlot.OffHand, itemPath: offHandPath.description)
        } else {
            offHandButton.enabled = false
        }
    }
    
    private func itemEquipped(path: String) -> Bool{
        if path != "null" {
            return true
        }
        return false
    }
    
    // set icon to view
    private func updateImages(){
        self.headButton.setImage(equipment[ItemSlot.Head], forState: .Normal)
        self.neckButton.setImage(equipment[ItemSlot.Neck], forState: .Normal)
        self.shoulderButton.setImage(equipment[ItemSlot.Shoulder], forState: .Normal)
        self.backButton.setImage(equipment[ItemSlot.Back], forState: .Normal)
        self.chestButton.setImage(equipment[ItemSlot.Chest], forState: .Normal)
        self.shirtButton.setImage(equipment[ItemSlot.Shirt], forState: .Normal)
        self.tabardButton.setImage(equipment[ItemSlot.Tabard], forState: .Normal)
        self.wristsButton.setImage(equipment[ItemSlot.Wrists], forState: .Normal)
        self.handsButton.setImage(equipment[ItemSlot.Hands], forState: .Normal)
        self.waistButton.setImage(equipment[ItemSlot.Waist], forState: .Normal)
        self.legsButton.setImage(equipment[ItemSlot.Legs], forState: .Normal)
        self.feetsButton.setImage(equipment[ItemSlot.Feets], forState: .Normal)
        self.finger1Button.setImage(equipment[ItemSlot.Finger1], forState: .Normal)
        self.finger2Button.setImage(equipment[ItemSlot.Finger2], forState: .Normal)
        self.trinket1Button.setImage(equipment[ItemSlot.Trinket1], forState: .Normal)
        self.trinket2Button.setImage(equipment[ItemSlot.Trinket2], forState: .Normal)
        self.mainHandButton.setImage(equipment[ItemSlot.MainHand], forState: .Normal)
        self.offHandButton.setImage(equipment[ItemSlot.OffHand], forState: .Normal)
    }
    
    // get character thumbnail from request
    private func loadCharacterImage(){
        let thumbnail = data[Character.Thumbnail]
        let profilemain = thumbnail.description.stringByReplacingOccurrencesOfString(RequestParameter.URL.Avatar, withString: RequestParameter.URL.ProfileMain, options: NSStringCompareOptions.LiteralSearch, range: nil)
        let downloadQueue = dispatch_queue_create("com.pro.asyncImages",nil)
        dispatch_async(downloadQueue){
            var data = NSData(contentsOfURL: NSURL(string: RequestParameter.URL.Image + profilemain)!)
            var image: UIImage?
            if (data != nil){
                image = UIImage(data: data!)
            }
            dispatch_async(dispatch_get_main_queue()){
                self.profileImageView.image = image
            }
        }
    }
    
    // get icon from request
    private func loadItemImage(type: String, itemPath: String){
        let downloadQueue = dispatch_queue_create("com.pro.asyncImages",nil)
        dispatch_async(downloadQueue){
            var data = NSData(contentsOfURL: NSURL(string: RequestParameter.URL.ItemImage + itemPath + RequestParameter.URL.JPG)!)
            var image: UIImage?
            if (data != nil){
                image = UIImage(data: data!)
            }
            dispatch_async(dispatch_get_main_queue()){
                self.equipment[type] = image
                self.updateImages()
                self.loadCharacterImage()
            }
        }
    }
    
    // favorite button
    @IBAction func tapGesture(sender: AnyObject) {
        addToFavorite()
    }
    
    @IBAction func showPopover(sender: UIButton) {
        performSegueWithIdentifier(Segue.ShowItem, sender: sender)
    }
    
    @IBAction func showPets() {
        performSegueWithIdentifier(Segue.ShowPets, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segue.ShowPets {
            if let pvc = segue.destinationViewController as? PetsViewController {
                pvc.name = self.characterName
                pvc.realm = self.characterRealm
            }
        }
        if segue.identifier == Segue.ShowItem {
            if let ivc = segue.destinationViewController as? ItemViewController{
                if let ppc = ivc.popoverPresentationController {
                    let minimumSize = ivc.view.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
                    ivc.preferredContentSize = CGSize(width: PopoverSettings.Width,height: PopoverSettings.Height)
                    if let x = sender?.frame.origin.x {
                        if let y = sender?.frame.origin.y {
                            ppc.sourceRect = CGRectMake(x + PopoverSettings.AddToRectangle, y + PopoverSettings.AddToRectangle, 0, 0)
                            switch sender!.tag {
                            case ButtonTag.Head:
                                ivc.itemId = getItemId(ItemSlot.Head)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Head]
                            case ButtonTag.Neck:
                                ivc.itemId = getItemId(ItemSlot.Neck)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Neck]
                            case ButtonTag.Shoulder:
                                ivc.itemId = getItemId(ItemSlot.Shoulder)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Shoulder]
                            case ButtonTag.Back:
                                ivc.itemId = getItemId(ItemSlot.Back)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Back]
                            case ButtonTag.Chest:
                                ivc.itemId = getItemId(ItemSlot.Chest)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Chest]
                            case ButtonTag.Shirt:
                                ivc.itemId = getItemId(ItemSlot.Shirt)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Shirt]
                            case ButtonTag.Tabard:
                                ivc.itemId = getItemId(ItemSlot.Tabard)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Tabard]
                            case ButtonTag.Wrists:
                                ivc.itemId = getItemId(ItemSlot.Wrists)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Wrists]
                            case ButtonTag.Hands:
                                ivc.itemId = getItemId(ItemSlot.Hands)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Hands]
                            case ButtonTag.Waist:
                                ivc.itemId = getItemId(ItemSlot.Waist)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Waist]
                            case ButtonTag.Legs:
                                ivc.itemId = getItemId(ItemSlot.Legs)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Legs]
                            case ButtonTag.Feets:
                                ivc.itemId = getItemId(ItemSlot.Feets)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Feets]
                            case ButtonTag.Finger1:
                                ivc.itemId = getItemId(ItemSlot.Finger1)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Finger1]
                            case ButtonTag.Finger2:
                                ivc.itemId = getItemId(ItemSlot.Finger2)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Finger2]
                            case ButtonTag.Trinket1:
                                ivc.itemId = getItemId(ItemSlot.Trinket1)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Trinket1]
                            case ButtonTag.Trinket2:
                                ivc.itemId = getItemId(ItemSlot.Trinket2)
                                ivc.item = data[ItemSlot.Items][ItemSlot.Trinket2]
                            case ButtonTag.MainHand:
                                ivc.itemId = getItemId(ItemSlot.MainHand)
                                ivc.item = data[ItemSlot.Items][ItemSlot.MainHand]
                            case ButtonTag.OffHand:
                                ivc.itemId = getItemId(ItemSlot.OffHand)
                                ivc.item = data[ItemSlot.Items][ItemSlot.OffHand]
                            default:
                                ivc.itemId = getItemId(ItemSlot.Error)
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func getItemId(item: String) -> Int{
        var itemId = data[ItemSlot.Items][item]["id"]
        return itemId.intValue
    }
    
    private func addToFavorite(){
        let newFavorite = SearchCharacter(name: self.characterName, realm: self.characterRealm)
        var favoriteArray = loadFavorites()

        if favoritesAlreadyContainsObject(favoriteArray, newFavorite: newFavorite) == false{
            favoriteArray.append(newFavorite)
            saveFavorites(favoriteArray)
            self.addToFavouriteImageView.image = UIImage(named: (Favorite.YellowStar))
        }else{
            self.addToFavouriteImageView.image = UIImage(named: (Favorite.WhiteStar))
            deleteFavorite(newFavorite)
        }

    }
    
    private func saveFavorites(newFavorites: [SearchCharacter]){
        let newFavoritesData = NSKeyedArchiver.archivedDataWithRootObject(newFavorites)
        NSUserDefaults.standardUserDefaults().setObject(newFavoritesData, forKey: Favorite.FavoriteKey)
    }
    
    func loadFavorites() -> [SearchCharacter]{
        let favoritesData = NSUserDefaults.standardUserDefaults().objectForKey(Favorite.FavoriteKey) as? NSData
        var returnFavoritesArray: [SearchCharacter] = []
        
        if let favoritesData = favoritesData {
            let favoritesArray = NSKeyedUnarchiver.unarchiveObjectWithData(favoritesData) as? [SearchCharacter]
            if let favoritesArray = favoritesArray {
                returnFavoritesArray = favoritesArray
            }
        }
        return returnFavoritesArray
    }
    
    private func favoritesAlreadyContainsObject(favoriteArray: [SearchCharacter], newFavorite: SearchCharacter) -> Bool{
        if favoriteArray.count > 0 {
            for var i = 0; i < favoriteArray.count; i++ {
                if favoriteArray[i].name == newFavorite.name && favoriteArray[i].realm == newFavorite.realm{
                    return true
                }
            }
        }else{
            return false
        }
        return false
    }
    
    func deleteFavorite(favoriteToDelete: SearchCharacter){
        var favorites = loadFavorites()
        var newFavorites: [SearchCharacter] = []
        for var i = 0; i < favorites.count; i++ {
            if favorites[i].name != favoriteToDelete.name && favorites[i].realm != favoriteToDelete.realm {
                newFavorites.append(favorites[i])
            }
        }
        saveFavorites(newFavorites)
    }

    private struct Favorite {
        static let WhiteStar = "star_grey_icon"
        static let YellowStar = "star_yellow_icon"
        static let FavoriteKey = "FavoritesTestKey"
    }
    
    private struct ItemSlot {
        static let Items = "items"
        static let Icon = "icon"
        static let Head = "head"
        static let Neck = "neck"
        static let Shoulder = "shoulder"
        static let Back = "back"
        static let Chest = "chest"
        static let Shirt = "shirt"
        static let Tabard = "tabard"
        static let Wrists = "wrist"
        static let Hands = "hands"
        static let Waist = "waist"
        static let Legs = "legs"
        static let Feets = "feet"
        static let Finger1 = "finger1"
        static let Finger2 = "finger2"
        static let Trinket1 = "trinket1"
        static let Trinket2 = "trinket2"
        static let MainHand = "mainHand"
        static let OffHand = "offHand"
        static let Error = "Error"
    }
    
    private struct Character {
        static let Thumbnail = "thumbnail"
        static let Name = "name"
        static let Level = "level"
        static let Class = "class"
        static let Race = "race"
        static let Classes = [1:"Krieger",2:"Paladin",3:"Jäger",4:"Schurke",5:"Priester",6:"Todesritter",7:"Schamane",8:"Magier",9:"Hexenmeister",10:"Mönch",11:"Druide"]
    }
    
    private struct ButtonTag {
        static let Head = 1
        static let Neck = 2
        static let Shoulder = 3
        static let Back = 4
        static let Chest = 5
        static let Shirt = 6
        static let Tabard = 7
        static let Wrists = 8
        static let Hands = 9
        static let Waist = 10
        static let Legs = 11
        static let Feets = 12
        static let Finger1 = 13
        static let Finger2 = 14
        static let Trinket1 = 15
        static let Trinket2 = 16
        static let MainHand = 17
        static let OffHand = 18
    }
    
    private struct Segue {
        static let ShowItem = "Show Item"
        static let ShowPets = "Show Pets"
    }
    
    private struct PopoverSettings {
        static let Height = 300
        static let Width = 320
        static let AddToRectangle: CGFloat = 18
    }
}
