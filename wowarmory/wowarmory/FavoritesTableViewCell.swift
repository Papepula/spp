//
//  FavoritesTableViewCell.swift
//  wowarmory
//
//  Created by admin on 04.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit

class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var characterNameTextLabel: UILabel!
    @IBOutlet weak var realmNameTextLabel: UILabel!
    @IBOutlet weak var searchCharacterImageView: UIImageView!
}
