//
//  FavoritesTableViewController.swift
//  wowarmory
//
//  Created by admin on 04.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController {
    
    @IBOutlet var favoritesTableView: UITableView!
    var favorites: [SearchCharacter] = []
    var showCharacterViewController = ShowCharacterViewController()
    var favoritesCounter = 0
    var name: String = ""
    var realm: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(animated: Bool) {
        updateUI()
    }
    
    func updateUI(){
        favorites = showCharacterViewController.loadFavorites()
        favoritesCounter = favorites.count
        self.favoritesTableView.reloadData()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favoritesCounter
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CustomCell.Favorite, forIndexPath: indexPath) as! FavoritesTableViewCell
        let row = indexPath.row
        cell.characterNameTextLabel.text = favorites[row].name
        cell.realmNameTextLabel.text = favorites[row].realm
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.dequeueReusableCellWithIdentifier(CustomCell.Favorite, forIndexPath: indexPath) as! FavoritesTableViewCell
        let row = indexPath.row
        self.name = self.favorites[row].name
        self.realm = self.favorites[row].realm
        performSegueWithIdentifier(Segue.SearchFavorite, sender: nil)
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]?  {
        var deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: CustomCell.FavoriteDeleteTitle , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            let row = indexPath.row
            var favoriteToDelete = SearchCharacter(name: self.favorites[row].name, realm: self.favorites[row].realm)
            self.showCharacterViewController.deleteFavorite(favoriteToDelete)
            self.updateUI()
        })
        return [deleteAction]
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if self.name != "" && self.realm != "" {
            if segue.identifier == Segue.SearchFavorite {
                if let navcon = segue.destinationViewController as? UINavigationController{
                    if let scvc = navcon.topViewController as? ShowCharacterViewController{
                        scvc.characterName = self.name
                        scvc.characterRealm = self.realm
                    }
                }
            }
        }
    }
    
    private struct Segue {
        static let SearchFavorite = "Search Favorite"
    }
    
    private struct CustomCell {
        static let Favorite = "FavoriteCell"
        static let FavoriteDeleteTitle = "Löschen"
    }
}
