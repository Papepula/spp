//
//  RequestParameter.swift
//  wowarmory
//
//  Created by admin on 15.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class RequestParameter {
    
    struct URL {
        static let ApiKey = "&apikey=62njm3fkqkez2rexwa3qygem43xrbavq"
        static let Character = "https://eu.api.battle.net/wow/character/"
        static let Item = "https://eu.api.battle.net/wow/item/"
        static let ItemClasses = "https://eu.api.battle.net/wow/data/item/classes"
        static let Items = "?fields=items"
        static let Image = "http://eu.battle.net/static-render/eu/"
        static let ProfileMain = "profilemain"
        static let Inset = "inset"
        static let Avatar = "avatar"
        static let ItemImage = "http://us.media.blizzard.com/wow/icons/36/"
        static let CharacterNotFound = "Character not found."
        static let RequestStatus = "nok"
        static let RequestFailed = "Charakter wurde nicht gefunden"
        static let Leaderboard = "https://eu.api.battle.net/wow/leaderboard/"
        static let RealmStatus = "https://eu.api.battle.net/wow/realm/status"
        static let Pets = "?fields=pets"
        static let PetSlots = "?fields=petSlots"
        static let PetImage = "http://us.media.blizzard.com/wow/icons/56/"
        static let Locale = "?locale=de_DE"
        static let AndLocale = "&locale=de_DE"
        static let PetRequestFailed = "Keine Haustiere vorhanden"
        static let JPG = ".jpg"
    }
}