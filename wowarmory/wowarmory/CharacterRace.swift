//
//  CharacterRaces.swift
//  wowarmory
//
//  Created by admin on 05.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class CharacterRace {
    
    var id: Int = 0
    var mask: Int = 0
    var side: String = ""
    var name: String = ""
    
    init(id: Int, mask: Int, side: String, name: String){
        self.id = id
        self.mask = mask
        self.side = side
        self.name = name
    }
}