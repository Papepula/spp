//
//  SecondViewController.swift
//  wowarmory
//
//  Created by admin on 24.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit

class LadderViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func selectLadder(sender: UIButton) {
        let ladder = sender.currentTitle
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier!{
        case Ladder.BracketType2v2:
            if let navcon = segue.destinationViewController as? UINavigationController{
                if let ltvc = navcon.topViewController as? LadderTableViewController{
                    ltvc.bracketType = Ladder.BracketType2v2
                }
            }
        case Ladder.BracketType3v3:
            if let navcon = segue.destinationViewController as? UINavigationController{
                if let ltvc = navcon.topViewController as? LadderTableViewController{
                    ltvc.bracketType = Ladder.BracketType3v3
                }
            }
        case Ladder.BracketType5v5:
            if let navcon = segue.destinationViewController as? UINavigationController{
                if let ltvc = navcon.topViewController as? LadderTableViewController{
                    ltvc.bracketType = Ladder.BracketType5v5
                }
            }
        case Ladder.BracketTypeRBG:
            if let navcon = segue.destinationViewController as? UINavigationController{
                if let ltvc = navcon.topViewController as? LadderTableViewController{
                    ltvc.bracketType = Ladder.BracketTypeRBG
                }
            }
        default:
            errorMessage()
        }
    }
    
    private func errorMessage() {
        var alert = UIAlertView()
        alert.title = "Fehler"
        alert.message = "Die Rangliste konnte nicht angezeigt werden"
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    private struct Ladder{
        static let BracketType2v2 = "2v2"
        static let BracketType3v3 = "3v3"
        static let BracketType5v5 = "5v5"
        static let BracketTypeRBG = "rbg"
    }
}

