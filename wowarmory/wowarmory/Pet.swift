//
//  Pet.swift
//  wowarmory
//
//  Created by admin on 11.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class Pet {
    
    var name: String = ""
    var qualityId: Int = 0
    var icon: String = ""
    var speciesId: Int = 0
    var breedId: Int = 0
    var level: Int = 0
    var health: Int = 0
    var power: Int = 0
    var speed: Int = 0
    var creatureName: String = ""
    var isFirstAbilitySlotSelected: Bool = false
    var isSecondAbilitySlotSelected: Bool = false
    var isThirdAbilitySlotSelected: Bool = false
    
    init(name: String, qualityId: Int, icon: String, speciesId: Int, breedId: Int, level: Int, health: Int, power: Int, speed: Int, creatureName: String, firstAbility: Bool, secondAbility: Bool, thirdAbility: Bool){
        self.name = name
        self.qualityId = qualityId
        self.icon = icon
        self.speciesId = speciesId
        self.breedId = breedId
        self.level = level
        self.health = health
        self.power = power
        self.speed = speed
        self.creatureName = creatureName
        self.isFirstAbilitySlotSelected = firstAbility
        self.isSecondAbilitySlotSelected = secondAbility
        self.isThirdAbilitySlotSelected = thirdAbility
    }
}
