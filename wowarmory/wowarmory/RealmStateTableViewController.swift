//
//  RealmStateViewController.swift
//  wowarmory
//
//  Created by admin on 24.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RealmStateTableViewController: UITableViewController {
    
    @IBOutlet var realmStateTableView: UITableView!
    @IBOutlet weak var spinner: UIRefreshControl!
    var requestData: JSON = JSON.nullJSON
    var realmData: JSON = JSON.nullJSON
    var realmState: [RealmState] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        executeRequest()
    }
    
    private func executeRequest(){
        spinner.beginRefreshing()
        Alamofire.request(.GET, RequestParameter.URL.RealmStatus + RequestParameter.URL.Locale + RequestParameter.URL.ApiKey)
            .responseJSON{ (request, response, json, error) in
                if json != nil {
                    var jsonObj = JSON(json!)
                    self.requestData = jsonObj
                    self.realmData = self.requestData["realms"]
                    self.setRealmStateModel()
                    self.spinner.endRefreshing()
                    self.realmStateTableView.reloadData()
                } else {
                    self.spinner.endRefreshing()
                }
        }
    }
    
    @IBAction func refresh(sender: UIRefreshControl) {
        executeRequest()
    }
    
    private func setRealmStateModel(){
        for var i = 0; i < realmData.count; i++ {
            var name = realmData[i][Realm.Name]
            var type = realmData[i][Realm.RealmType]
            var population = realmData[i][Realm.Population]
            var status = realmData[i][Realm.Status]
            realmState.append(RealmState(name: name.description, type: type.description, population: population.description, status: status.description))
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmData.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CustomCell.RealmState, forIndexPath: indexPath) as! RealmStateTableViewCell

        var row = indexPath.row

        if realmData != nil {
            cell.realmNameLabel.text = realmState[row].name
            cell.realmTypeLabel.text = realmState[row].type.uppercaseString
            cell.realmPopulationLabel.text = realmState[row].population
            
            switch realmState[row].status{
                case Realm.IsOnline:
                    cell.realmStatusImageView.image = UIImage(named: Realm.Online)
                default:
                    cell.realmStatusImageView.image = UIImage(named: Realm.Offline)
            }
        }
        return cell
    }
    
    private struct Realm {
        static let HighPopulation = "high"
        static let IsOnline = "true"
        static let IsOffline = "false"
        static let Online = "online"
        static let Offline = "offline"
        static let Name = "name"
        static let RealmType = "type"
        static let Population = "population"
        static let Status = "status"
    }
    
    private struct CustomCell {
        static let RealmState = "Realm State"
    }
}
