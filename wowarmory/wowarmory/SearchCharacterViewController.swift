//
//  FirstViewController.swift
//  wowarmory
//
//  Created by admin on 24.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit
import Alamofire

class SearchCharacterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var characterTextField: UITextField!
    @IBOutlet weak var realmTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier!{
            case Segue.SearchCharacter:
                if let navcon = segue.destinationViewController as? UINavigationController{
                    if let scvc = navcon.topViewController as? ShowCharacterViewController{
                        if let character = characterTextField.text {
                            if let realm = realmTextField.text {
                                let escapedCharacter = character.stringByReplacingOccurrencesOfString(" ", withString: "")
                                let escapedRealm = realm.stringByReplacingOccurrencesOfString(" ", withString: "")
                                realm.stringByReplacingOccurrencesOfString(" ", withString: "")
                                if escapedCharacter != "" && escapedRealm != "" {
                                    scvc.characterName = escapedCharacter.capitalizedString
                                    scvc.characterRealm = escapedRealm.capitalizedString
                                }
                            }
                        }
                    }
                }
            default:
                errorMessage()
        }
    }
    
    private func errorMessage() {
        var alert = UIAlertView()
        alert.title = "Fehler"
        alert.message = "Es ist ein Fehler aufgetreten"
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    private struct Segue {
        static let SearchCharacter = "Search Character"
    }
}



