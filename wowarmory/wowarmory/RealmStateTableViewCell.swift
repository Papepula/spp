//
//  RealmStateTableViewCell.swift
//  wowarmory
//
//  Created by admin on 26.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit

class RealmStateTableViewCell: UITableViewCell {

    @IBOutlet weak var realmNameLabel: UILabel!
    @IBOutlet weak var realmTypeLabel: UILabel!
    @IBOutlet weak var realmPopulationLabel: UILabel!
    @IBOutlet weak var realmStatusImageView: UIImageView!
}
