//
//  LadderCharacter.swift
//  wowarmory
//
//  Created by admin on 11.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class LadderCharacter {
    
    var name: String = ""
    var realm: String = ""
    var seasonWins: Int = 0
    var seasonLosses: Int = 0
    var rating: Int = 0
    var raceId: Int = 0
    var classId: Int = 0
    var fractionId: Int = 0
    var ranking: Int = 0
    
    init(name: String, realm: String, seasonWins: Int, seasonLosses: Int, rating: Int, raceId: Int, classId: Int, fractionId: Int, ranking: Int){
        self.name = name
        self.realm = realm
        self.seasonWins = seasonWins
        self.seasonLosses = seasonLosses
        self.rating = rating
        self.raceId = raceId
        self.classId = classId
        self.fractionId = fractionId
        self.ranking = ranking
    }
}