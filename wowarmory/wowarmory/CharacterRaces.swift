//
//  CharacterRaces.swift
//  wowarmory
//
//  Created by admin on 05.05.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class CharacterRaces {
    
    var characterRaces: [CharacterRace] = []
    
    init(){
        characterRaces.append(CharacterRace(id: 1, mask: 1, side: "alliance", name: "Mensch"))
        characterRaces.append(CharacterRace(id: 2, mask: 2, side: "horde", name: "Orc"))
        characterRaces.append(CharacterRace(id: 3, mask: 4, side: "alliance", name: "Zwerg"))
        characterRaces.append(CharacterRace(id: 4, mask: 8, side: "alliance", name: "Nachtelf"))
        characterRaces.append(CharacterRace(id: 5, mask: 16, side: "horde", name: "Untoter"))
        characterRaces.append(CharacterRace(id: 6, mask: 32, side: "horde", name: "Tauren"))
        characterRaces.append(CharacterRace(id: 7, mask: 64, side: "alliance", name: "Gnom"))
        characterRaces.append(CharacterRace(id: 8, mask: 128, side: "horde", name: "Troll"))
        characterRaces.append(CharacterRace(id: 9, mask: 256, side: "horde", name: "Goblin"))
        characterRaces.append(CharacterRace(id: 10, mask: 512, side: "horde", name: "Blutelf"))
        characterRaces.append(CharacterRace(id: 11, mask: 1024, side: "alliance", name: "Draenei"))
        characterRaces.append(CharacterRace(id: 22, mask: 2097152, side: "alliance", name: "Worgen"))
        characterRaces.append(CharacterRace(id: 24, mask: 8388608, side: "neutral", name: "Pandaren"))
        characterRaces.append(CharacterRace(id: 25, mask: 16777216, side: "alliance", name: "Pandaren"))
        characterRaces.append(CharacterRace(id: 26, mask: 33554432, side: "horde", name: "Pandaren"))
    }
}