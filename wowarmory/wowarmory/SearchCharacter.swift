//
//  SearchCharacter.swift
//  wowarmory
//
//  Created by admin on 25.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class SearchCharacter : NSObject{
    
    var name: String = ""
    var realm: String = ""
    
    init(name: String, realm: String){
        self.name = name
        self.realm = realm
    }
    
    required init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObjectForKey(Key.Name) as! String
        self.realm = aDecoder.decodeObjectForKey(Key.Realm) as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(name, forKey: Key.Name)
        aCoder.encodeObject(realm, forKey: Key.Realm)
    }
    
    private struct Key {
        static let Name = "name"
        static let Realm = "realm"
    }
}