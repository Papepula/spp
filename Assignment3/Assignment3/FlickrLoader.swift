//
//  InstagramLoader.swift
//  Assignment3
//
//  Created by Andreas Partenhauser on 01.04.15.
//  Copyright (c) 2015 Chip Digital GmbH. All rights reserved.
//

import UIKit
import Alamofire

public class FlickrLoader: NSObject {
    let FlickrRestBaseUrl = "https://api.flickr.com/services/rest/"
    let FlickrAPIKey = "fd2b03f1a74de7a45ebd5c1d54bc2789"
    let FlickrSecretKey = "c63b8995ba8f0380"
    
    let FormatNameJson = "json"
    let NoJsonCallBackValue = "1"
    
    let manager = Alamofire.Manager.sharedInstance
    
    override init() {
        super.init()
    }
    
    public class var sharedInstance: FlickrLoader {
        struct Singleton {
            static let instance = FlickrLoader()
        }
        
        return Singleton.instance
    }
    
    // ?method=flickr.photos.search&api_key=fd2b03f1a74de7a45ebd5c1d54bc2789&text=Rosenheim
    func loadPhotos(forSearchString searchString: String) {
        let parameter = [ParameterName.ApiKey: FlickrAPIKey, ParameterName.Method: Methods.PhotoSearchMethod, ParameterName.Format: FormatNameJson, ParameterName.Text: searchString, ParameterName.NoJsonCallback: NoJsonCallBackValue]
        
        let urlString = urlWithParameter(parameter)
        
        if let url = NSURL(string: urlString) {
            loadRequest(url, notifificationName: Methods.PhotoSearchMethod)
        }
    }
    
    // ?method=flickr.photos.geo.getLocation&api_key=aaaa62bf4a58fb3d6cd45ab19ba43d0e&photo_id=17292833231
    func getLocation(forSearchString searchString: String) {
        let parameter = [ParameterName.ApiKey: FlickrAPIKey, ParameterName.Method: Methods.PhotoLocationMethod, ParameterName.Format: FormatNameJson, ParameterName.PhotoId: searchString, ParameterName.NoJsonCallback: NoJsonCallBackValue]
        
        let urlString = urlWithParameter(parameter)
        
        if let url = NSURL(string: urlString) {
            loadRequest(url, notifificationName: Methods.PhotoLocationMethod)
        }
    }
    
    private func loadRequest(url: NSURL, notifificationName: String) {
        manager.request(NSURLRequest(URL: url)).responseJSON(options: NSJSONReadingOptions.AllowFragments, completionHandler: { (_, _, data, _) -> Void in
            self.handleResponse(data as! Dictionary<String, AnyObject>, notifificationName: notifificationName)
        })
    }
    
    private func handleResponse(data: Dictionary<String, AnyObject>, notifificationName: String) {
        NSNotificationCenter.defaultCenter().postNotificationName(notifificationName, object: nil, userInfo: [UserInfoKeys.DataKey: data])
    }
    
    private func urlWithParameter(parameter: Dictionary<String, String>) -> String {
        var urlString = "\(FlickrRestBaseUrl)?"
        for (parameterName, parameterValue) in parameter {
            urlString = urlString + parameterName + "=" + parameterValue + "&"
        }
        return urlString
    }
}

struct UserInfoKeys {
    static let DataKey = "DataKey"
}

struct Methods {
    static let PhotoSearchMethod = "flickr.photos.search"
    static let PhotoLocationMethod = "flickr.photos.geo.getLocation"
    static let PhotoInfoMethod = "flickr.photos.getInfo"
}

struct ParameterName {
    static let ApiKey = "api_key"
    static let Method = "method"
    static let Text = "text"
    static let Format = "format"
    static let PhotoId = "photo_id"
    static let NoJsonCallback = "nojsoncallback"
}
