//
//  EditViewController.swift
//  Assignment3
//
//  Created by admin on 28.04.15.
//  Copyright (c) 2015 Chip Digital GmbH. All rights reserved.
//

import UIKit

class EditViewController: UIViewController, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var imageScrollView: UIScrollView!
    var imageView = UIImageView()
    
    var serverId: String = ""
    var photoId: String = ""
    var farmId: Int = 0
    var secretId: String = ""
    var currentImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let downloadQueue = dispatch_queue_create("com.pro.asyncImages",nil)
        dispatch_async(downloadQueue){
            var data = NSData(contentsOfURL: NSURL(string: "https://farm\(self.farmId).staticflickr.com/\(self.serverId)/\(self.photoId)_\(self.secretId)_b.jpg")!)
            
            var image: UIImage?
            if (data != nil){
                image = UIImage(data: data!)
            }
            dispatch_async(dispatch_get_main_queue()){
                self.currentImage = image!
                self.loadImage()
            }
        }
    }
    
    
    func loadImage(){
        imageScrollView.delegate = self
        imageView.frame = CGRectMake(0, 0, currentImage.size.width, currentImage.size.height)
        imageView.image = self.currentImage
        imageView.userInteractionEnabled = true
        imageScrollView.addSubview(imageView)
        
        imageScrollView.contentSize = self.currentImage.size
        let scrollViewFrame = imageScrollView.frame
        let scaleWidth = scrollViewFrame.size.width / imageScrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / imageScrollView.contentSize.height
        let minScale = min(scaleHeight, scaleWidth)

        imageScrollView.minimumZoomScale = minScale
        imageScrollView.maximumZoomScale = 2
        imageScrollView.zoomScale = minScale
        
        centerScrollViewContents()
        
    }

    func centerScrollViewContents(){
        let boundsSize = imageScrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width{
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2
        }else{
            contentsFrame.origin.x = 0
        }
        
        if contentsFrame.size.height < boundsSize.height{
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2
        }else{
            contentsFrame.origin.y = 0
        }
        
        imageView.frame = contentsFrame
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        centerScrollViewContents()
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    
}
