//
//  ImageTableViewCell.swift
//  Assignment3
//
//  Created by admin on 27.04.15.
//  Copyright (c) 2015 Chip Digital GmbH. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var myLabel: UILabel!
    
}
