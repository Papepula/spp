//
//  ViewController.swift
//  Assignment3
//
//  Created by Andreas Partenhauser on 01.04.15.
//  Copyright (c) 2015 Chip Digital GmbH. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleSearchResponse:", name: Methods.PhotoSearchMethod, object: nil)
        
        FlickrLoader.sharedInstance.loadPhotos(forSearchString: "Rosenheim")
    }

    func handleSearchResponse(notification: NSNotification) {
        if let response =  notification.userInfo?[UserInfoKeys.DataKey] as? Dictionary<String, AnyObject> {
            for (key, value) in response {
                println("Key: \(key)")
                println("Value: \(value)")
            }
        }
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Show Image"{
            if let tvc = segue.destinationViewController as? ImageTableViewController{
                tvc.searchText = searchTextField.text
            }
        }
    }

}

