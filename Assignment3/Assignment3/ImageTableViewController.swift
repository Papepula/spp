//
//  ImageTableViewController.swift
//  Assignment3
//
//  Created by admin on 27.04.15.
//  Copyright (c) 2015 Chip Digital GmbH. All rights reserved.
//

import UIKit
import Alamofire

class ImageTableViewController: UITableViewController {

    @IBOutlet var imageTableView: UITableView!
    var searchText: String = ""
    @IBOutlet weak var searchImageView: UIImageView!
    var responseData: Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleSearchResponse:", name: Methods.PhotoSearchMethod, object: nil)

        if searchText != ""{
            FlickrLoader.sharedInstance.loadPhotos(forSearchString: searchText)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Show Map" {
            if let navcon = segue.destinationViewController as? UINavigationController{
                if let mvc = navcon.topViewController as? MapViewController {
                    if let blogIndex = self.imageTableView.indexPathForSelectedRow()?.row {
                        if let data = responseData["photos"] as? NSDictionary {
                            if let images = data["photo"] as? NSArray{
                                if let image = images[blogIndex] as? NSDictionary{
                                    if let imageId = image["id"] as? String{
                                        if let server = image["server"] as? String{
                                            if let farm = image["farm"] as? Int{
                                                if let photo = image["id"] as? String{
                                                    if let secret = image["secret"] as? String{
                                                        mvc.serverId = server
                                                        mvc.imageId = imageId
                                                        mvc.photoId = photo
                                                        mvc.farmId = farm
                                                        mvc.secretId = secret
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func updateUI(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
    }
    
    func handleSearchResponse(notification: NSNotification) {
        if let response =  notification.userInfo?[UserInfoKeys.DataKey] as? Dictionary<String, AnyObject> {
            for (key, value) in response {
                println("Key: \(key)")
                println("Value: \(value)")
            }
            responseData = response
            updateUI()
        }
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let images = responseData["photos"] as? NSDictionary {
            if let image = images["photo"] as? NSArray{
                return image.count
            }
        }
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MyCell", forIndexPath: indexPath) as! ImageTableViewCell
    
        var row = indexPath.row
        
        if let data = responseData["photos"] as? NSDictionary {
            if let images = data["photo"] as? NSArray{
                if let image = images[row] as? NSDictionary{
                    if let imageTitle = image["title"] as? String{
                        cell.myLabel.text = imageTitle
                        if let farmId = image["farm"] as? Int{
                            if let serverId = image["server"] as? String{
                                if let photoId = image["id"] as? String{
                                    if let secretId = image["secret"] as? String{

                                        let downloadQueue = dispatch_queue_create("com.pro.asyncImages",nil)
                                        dispatch_async(downloadQueue){
                                            
                                            var data = NSData(contentsOfURL: NSURL(string: "https://farm\(farmId).staticflickr.com/\(serverId)/\(photoId)_\(secretId)_m.jpg")!)
                                            
                                            var image: UIImage?
                                            if (data != nil){
                                                image = UIImage(data: data!)
                                            }
                                            dispatch_async(dispatch_get_main_queue()){
                                                cell.myImageView.image = image
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return cell
    }
}
