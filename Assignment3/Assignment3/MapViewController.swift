//
//  MapViewController.swift
//  Assignment3
//
//  Created by admin on 27.04.15.
//  Copyright (c) 2015 Chip Digital GmbH. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    let locationManager = CLLocationManager()
    @IBOutlet weak var distanceTextLabel: UILabel!
    @IBOutlet weak var editBarButtonItem: UIBarButtonItem!
    
    var imageLatitude: Double = 0
    var imageLongitude: Double = 0
    var responseData: Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    var imageId: String = ""
    let LatDelta:CLLocationDegrees = 0.01
    let LongDelta:CLLocationDegrees = 0.01
    let LeftCalloutFrame = CGRect(x: 0, y: 0, width: 59, height: 59)
    
    var serverId: String = ""
    var photoId: String = ""
    var farmId: Int = 0
    var secretId: String = ""
    
    @IBOutlet weak var mapView: MKMapView!{
        didSet{
            mapView.mapType = .Satellite
            mapView.delegate = self
            mapView.frame = self.mapView.bounds
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        mapView.frame = self.mapView.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        if imageId != ""{
            println("imageId \(imageId)")
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleSearchResponse:", name: Methods.PhotoLocationMethod, object: nil)
            FlickrLoader.sharedInstance.getLocation(forSearchString: imageId)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
      
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(manager.location, completionHandler: { (placemarks, e) -> Void in
            if let error = e {
                println("Error:  \(e.localizedDescription)")
            } else {
                let placemark = placemarks.last as! CLPlacemark
                
                let userInfo = [
                    "city":     placemark.locality,
                    "state":    placemark.administrativeArea,
                    "country":  placemark.country
                ]
                
                //println("Location:  \(userInfo)")
                
                let location = locations.last as! CLLocation
                let latitude = location.coordinate.latitude
                let longitude = location.coordinate.longitude
                
                let myLocation = location.coordinate
                let imageLocation = CLLocationCoordinate2DMake(self.imageLatitude, self.imageLongitude)
                
                if self.imageLatitude == 0.0 && self.imageLongitude == 0.0 {
                    self.distanceTextLabel.text = "No Location"
                } else {
                    var distance = self.calculateDistance(myLocation, to: imageLocation)
                    var distanceInKM: Int = Int(distance / 1000)
                    self.distanceTextLabel.text = distanceInKM.description + " km"
                }
            }
        })
        
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error: " + error.localizedDescription)
    }
    
    func calculateDistance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> Double{
    
    let DEG_TO_RAD = 0.017453292519943295769236907684886;
    let EARTH_RADIUS_IN_METERS = 6372797.560856;
    
    var latitudeArc  = (from.latitude - to.latitude) * DEG_TO_RAD;
    var longitudeArc = (from.longitude - to.longitude) * DEG_TO_RAD;
    var latitudeH = sin(latitudeArc * 0.5);
    latitudeH *= latitudeH;
    var lontitudeH = sin(longitudeArc * 0.5);
    lontitudeH *= lontitudeH;
    var tmp = cos(from.latitude*DEG_TO_RAD) * cos(to.latitude*DEG_TO_RAD);
    return EARTH_RADIUS_IN_METERS * 2.0 * asin(sqrt(latitudeH + tmp*lontitudeH));
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Show Big Image" {
            if let navcon = segue.destinationViewController as? UINavigationController{
                if let evc = navcon.topViewController as? EditViewController {
                    evc.secretId = self.secretId
                    evc.serverId = self.serverId
                    evc.photoId = self.photoId
                    evc.farmId = self.farmId
                }
            }
        }
    }
    
    func handleSearchResponse(notification: NSNotification) {
        if let response =  notification.userInfo?[UserInfoKeys.DataKey] as? Dictionary<String, AnyObject> {
            for (key, value) in response {
                println("Key: \(key)")
                println("Value: \(value)")
            }
            responseData = response
            updateUI()
        }
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        var view = mapView.dequeueReusableAnnotationViewWithIdentifier("Test")
        
        if view == nil{
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Test")
            view.canShowCallout = true
        } else {
            view.annotation = annotation
        }
        
        println("MY SERVER ID = \(serverId)")
        println("MY PHOTO ID = \(photoId)")
        println("MY SECRET ID = \(secretId)")
        println("MY FARM ID = \(farmId)")
        
        view.leftCalloutAccessoryView = UIImageView(frame: LeftCalloutFrame)
        
        return view
    }
    
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
        
        if let thumbnailImageView = view.leftCalloutAccessoryView as? UIImageView{
            
            let downloadQueue = dispatch_queue_create("com.pro.asyncImages",nil)
            dispatch_async(downloadQueue){
                var data = NSData(contentsOfURL: NSURL(string: "https://farm\(self.farmId).staticflickr.com/\(self.serverId)/\(self.photoId)_\(self.secretId)_s.jpg")!)
                
                var image: UIImage?
                if (data != nil){
                    image = UIImage(data: data!)
                }
                dispatch_async(dispatch_get_main_queue()){
                    thumbnailImageView.image = image
                }
            }
        }
    }

    func updateUI(){
        if let data = responseData["photo"] as? NSDictionary {
            println("DATA \(data)")
            if let location = data["location"] as? NSDictionary{
                println("LOCATION \(location)")
                if let lat = location["latitude"] as? NSString{
                    if let long = location["longitude"] as? NSString{

                        self.imageLatitude = lat.doubleValue
                        self.imageLongitude = long.doubleValue
                        println("latitude \(self.imageLatitude)")
                        println("latitude \(self.imageLongitude)")

                        // location
                        var span:MKCoordinateSpan = MKCoordinateSpanMake(LatDelta, LongDelta)
                        var currentLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.imageLatitude,self.imageLongitude)
                        var region:MKCoordinateRegion = MKCoordinateRegionMake(currentLocation, span)
                        self.mapView.setRegion(region, animated: true)
                        
                        // pin
                        var currentAnnotation = MKPointAnnotation()
                        currentAnnotation.coordinate = currentLocation
                        currentAnnotation.title = "Current Image Location"
                        self.mapView.addAnnotation(currentAnnotation)
                    }
                }
            }
        }
    }
}
