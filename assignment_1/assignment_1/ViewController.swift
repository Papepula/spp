//
//  ViewController.swift
//  assignment_1
//
//  Created by admin on 24.03.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import UIKit

protocol MyProtocol: class {
    func reset (label: UILabel)
}

class ViewController: UIViewController, MyProtocol {

    // meins
    weak var myProtocol: MyProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //seins
        //let textField = UITextField()
        //lbl_label.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var lbl_label: MyLabel!
    
    @IBAction func changeLabelColor(sender: UIButton) {
        var color = sender.currentTitleColor
        lbl_label.textColor = color
    }
    
    @IBAction func resetColor(sender: UITapGestureRecognizer) {
        //lbl_label.textColor = UIColor.blackColor()
        reset(lbl_label)
    }
    
    func reset(label: UILabel) {
        label.textColor = UIColor.blackColor()
    }
    
    @IBAction func changeLabelContent(sender: UITextField?) {

        if let text = sender?.text {
            lbl_label.text = text
        }
        
        //myProtocol?.reset(lbl_label)
        
        //sender!.text = nil
    }
    
}

