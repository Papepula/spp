//
//  MyLabel.swift
//  assignment_1
//
//  Created by admin on 30.03.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation
import UIKit

class MyLabel: UILabel {
    var delegate: MyProtocol?
    
    func mymethod() {
        delegate?.reset(self)
    }

}