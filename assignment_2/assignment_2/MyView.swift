//
//  MyView.swift
//  assignment_2
//
//  Created by admin on 06.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//
import UIKit

class MyView: UIViewController {
    
    var myModel = MyModel()
    
    @IBOutlet weak var btn_saveText: UIButton!
    @IBOutlet weak var txt_textToSave: UITextField!
    
    @IBAction func saveText() {
        if let text = txt_textToSave.text{
            if text != ""{
                myModel.data += [text]
                println(myModel.data)
            }
        }
    }
    
}
