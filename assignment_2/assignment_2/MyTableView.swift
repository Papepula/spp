//
//  MyTable.swift
//  assignment_2
//
//  Created by admin on 06.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//
import UIKit

class MyTableView: UITableViewController {

    var myModel = MyModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet var myTableView: UITableView!
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myModel.data.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MyCell", forIndexPath: indexPath) as! UITableViewCell
        
        cell.textLabel?.text = myModel.data[indexPath.row]
        
        return cell
    }
}