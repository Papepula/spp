//
//  MyModel.swift
//  assignment_2
//
//  Created by admin on 06.04.15.
//  Copyright (c) 2015 papepula. All rights reserved.
//

import Foundation

class MyModel {
    
    let DEFAULTS_KEY = "myData"
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var data: [String] {
        get{
            return defaults.objectForKey(DEFAULTS_KEY) as? [String] ?? []
        }
        set{
            defaults.setObject(newValue, forKey: DEFAULTS_KEY)
            defaults.synchronize()
        }
    }
}